﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    // Singleton
    public static GameManager instance;

    //Players lives
    public int lives;

    //Player
    public GameObject player;

    //List of enemies
    public List<GameObject> enemies;

    // List of active enemies
    public List<GameObject> activeEnemies;
    public int maxEnemies;

    //Enemy Speeds
    public float asteroidSpeed;
    public float enemyShipSpeed;

    //Bullet Data
    public GameObject bullet;
    public float bulletSpeed;
    public float bulletLife;

    //Spawn Points
    public GameObject[] spawnPoints;

    // Create the Singleton instance
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (activeEnemies.Count < maxEnemies)
        {
            SpawnEnemy();
        }
    }

    void SpawnEnemy()
    {

        // Determine spawn point
        int id = Random.Range(0, spawnPoints.Length);
        GameObject point = spawnPoints[id];

        // Determine which enemy to spawn
        GameObject enemy = enemies[Random.Range(0, enemies.Count)];

        // Instantiate an enemy
        GameObject enemyInstance = Instantiate<GameObject>(enemy, point.transform.position, Quaternion.identity);

        // Add to enemies list
        activeEnemies.Add(enemyInstance);

    }

    //Lose a life
    public void LifeLost()
    {
        lives--;
        if(lives <= 0)
        {
            Application.Quit();
        }
        else
        {
            player.transform.position = new Vector3(0, 0, 0);
            player.transform.rotation = new Quaternion(0, 0, 0, 0);

            for(int i = 0; i < activeEnemies.Count; i++)
            {
                Destroy(activeEnemies[i]);
            }
            activeEnemies.Clear();
        }
    }
}
