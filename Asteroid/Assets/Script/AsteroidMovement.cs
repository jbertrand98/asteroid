﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidMovement : MonoBehaviour
{

    Vector2 direction;
    // Start is called before the first frame update
    void Start()
    {
        //Aim for player when spawned
        direction = GameManager.instance.player.transform.position - transform.position;
        direction.Normalize();
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, 0f, angle);
    }

    // Update is called once per frame
    void Update()
    {
       //Move Forward
		transform.Translate(Time.deltaTime * GameManager.instance.asteroidSpeed, 0, 0);
    }
}
