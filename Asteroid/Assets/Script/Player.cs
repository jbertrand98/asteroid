﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    private Transform tf;
    public float moveSpeed;
    public float turnSpeed;

    // Start is called before the first frame update
    void Start()
    {
        //Get transform component
        tf = gameObject.GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        //Move the ship forward
        if (Input.GetKey("up") || Input.GetKey("w"))
        {
            tf.Translate(Vector3.up * moveSpeed * Time.deltaTime);
        }

        //turn the ship right
        if (Input.GetKey("right") || Input.GetKey("d"))
        {
            tf.Rotate(Vector3.back * turnSpeed * Time.deltaTime);
        }

        //turn the ship left
        if (Input.GetKey("left") || Input.GetKey("a"))
        {
            tf.Rotate(Vector3.forward * turnSpeed * Time.deltaTime);
        }

        if (Input.GetKeyDown("space"))
        {
            GameObject bullet = Instantiate<GameObject>(GameManager.instance.bullet);
            bullet.transform.position = transform.position;
        }
    }

    void OnCollisionEnter2D(Collision2D otherObject)
    {
        //Lose a life when hit
        if (otherObject.gameObject.tag == "Enemy")
        {
            GameManager.instance.LifeLost();
        }
    }
}
