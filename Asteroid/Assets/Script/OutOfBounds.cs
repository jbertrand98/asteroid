﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutOfBounds : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerExit2D(Collider2D otherObject)
    {
        if (otherObject.gameObject.tag == "Player")
        {
            GameManager.instance.LifeLost();
        }
        else if(otherObject.gameObject.tag == "Enemy")
        {
            GameManager.instance.activeEnemies.Remove(otherObject.gameObject);
            Destroy(otherObject.gameObject);
        }
    }
}
