﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    Vector3 direction;
    // Start is called before the first frame update
    void Start()
    {
        direction = GameManager.instance.player.transform.up;
        Destroy(this.gameObject, GameManager.instance.bulletLife);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(direction * Time.deltaTime * GameManager.instance.bulletSpeed);
    }

    void OnCollisionEnter2D(Collision2D otherObject)
    {
        if(otherObject.gameObject.tag == "Enemy")
        {
            GameManager.instance.activeEnemies.Remove(otherObject.gameObject);
            Destroy(otherObject.gameObject);
        }
    }
}
